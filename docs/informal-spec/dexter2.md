# Dexter 2 Exchange: Informal Specification

This document is the informal specification of Dexter 2, a smart contract
for decentralized token exchange for the Tezos block chain.

## Introduction

Three smart contracts are involved in a Dexter 2 decentralized
exchange: a Constant Product Market Maker (henceforth referred to as
"CPMM contract"), a target FA1.2/FA2 contract (henceforth referred to
as "token contract") managing a pool of tokens (henceforth referred to
as "TOK"), and an FA1.2 contract (henceforth referred to as "liquidity
token contract") managing liquidity tokens (henceforth referred to as
"LQT").

The main contract is the CPMM contract; a smart contract similar to
Ethereum's Uniswap decentralized exchange. Three kinds of users interact
with this contract.

The first kind of users are called traders; they can use the contract
to trade XTZ for TOK (using the `%xtzToToken` entrypoint) and vice-versa
they can trade TOK for XTZ (using the `%tokenToXtz` entrypoint). Traders
can also use the CPMM to trade TOK for another FA1.2/FA2 token (using the
`%tokenToToken` entrypoint).

In order to perform the trade without waiting for another trader
wanting to do the symmetric trade, the CPMM contract holds both XTZ
and TOK in so-called pools. Both are provided by the second kind of
user, called *liquidity providers*. For a given trade, the CPMM
computes an exchange rate based on its pools of TOK and XTZ. As a
consequence, the exchange rate is independent from other trades.

Liquidity providers deposit simultaneously XTZ and TOK tokens in the CPMM
pools, in exchange for LQT tokens (using the `%addLiquidity`
entrypoint). The share of the liquidity tokens that each liquidity
provider owns is materialized using the liquidity token contract
administered by the CPMM contract. Each time `%addLiquidity` is
called, new LQT are minted and credited to the liquidity provider that
called it. Liquidity providers can also withdraw an amount of XTZ and
TOK token by burning their LQT (using the `%removeLiquidity`
entrypoint).

In addition to traders and liquidity bakers, a designated user called
the manager (authentifying by their address) is allowed to set the
delegate of the CPMM contract (through the `%setBaker` entrypoint);
set the address of the liquidity token contract associated to the
CPMM (through the `%setLqtAddress` entrypoint); and change the manager
(through the `%setManager` entrypoint).

Finally, anyone can make a donation in XTZ to the CPMM contract (using
the `%default` entrypoint).  This donation goes to the XTZ part of the
liquidity pool but no LQT is minted for it so users have no reason to
call this entrypoint; the sent XTZ are effectively shared between the
liquidity providers.

To incentivize liquidity providers, for each trade, a 0.3% fee of the
traded amount is also sent to the liquidity pool.

## Contracts

Dexter 2 is composed of two Tezos smart contracts:

 - `dexter.mligo` (`dexter.mligo.tz`, contract hash
   `expruahNDMqQxdsrBoyUF7pC1z6qGSRofgtFtgL8MgTsAe74n8FSdc`):
   implements the CPMM contract.
 - `lqt_fa12.mligo` (`lqt_fa12.mligo.tz`, contract hash
   `exprufAK15C2FCbxGLCEVXFe26p3eQdYuwZRk1morJUwy9NBUmEZVB`):
   implements the liquidity token contract as a tradable FA1.2 token.

## Variants

Dexter 2 comes in two variants:

 - Dexter 2/FA1.2: this variant enables decentralized exchange by pairing with a FA1.2 tokens
 - Dexter 2/FA2: this variant enables decentralized exchange by pairing with a FA2 tokens

## Informal specification

 - For a review of the terminology used in the Dexter 2 informal
   specifications, see [`dexter2-terminology`][dexter2-terminology].
 - For the informal specification of the CPPM contract, see
   [`dexter2-cpmm`][dexter2-cpmm].
 - For the informal specification of the liquidity token contract, see
   [`dexter2-lqt-fa12`][dexter2-lqt-fa12].

## Related documents

 - [The FA1.2 standard](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-7/tzip-7.md)
 - [The FA2 standard](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md)

[dexter2-cpmm]: dexter2-cpmm.md
[dexter2-terminology]: dexter2-terminology.md
[dexter2-lqt-fa12]: dexter2-lqt-fa12.md
